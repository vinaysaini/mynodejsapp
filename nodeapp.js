var express 	= require('express');
var app 		= express();
var port 		= process.env.PORT || 8085;
var bodyParser 	= require('body-parser');
var logger 		= require('express-logger');
var cookieParser = require('cookie-parser');
var passport 	= require('passport');
var flash    	= require('connect-flash');
var session 	= require('express-session');
var mongoose 	= require('mongoose');
var configDB 	= require('./config/database.js');
// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

app.use(logger({path: "./app/log.txt"}));
app.use((bodyParser.urlencoded({
  extended: true
})));

app.use(cookieParser());

app.use("/public",express.static(__dirname + "/public"));

// set the view engine to ejs
app.set('view engine', 'ejs');

app.use(session({ secret: 'abcdefghijklmnopqrstuvwxyz',resave: true,saveUninitialized: true })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
require('./config/passport')(passport); // pass passport for configuration


// routes ======================================================================
require('./app/routes.js')(app,passport); // load our routes and pass in our app and fully configured passport
// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);
